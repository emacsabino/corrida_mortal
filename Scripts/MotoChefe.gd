extends Area2D
var velocidade=300
var direcao = Vector2(0,1)
var vai_vem=false
var vivo = true
var pai 
var pontos=100
var dinamite = preload("res://Scenes/Objetos/Dinamite.tscn")

func _ready():
	pai= get_parent().get_parent()


func _process(delta):
	if PainelSingleton.get_parar_pista():
		vai_vem=false
		direcao.y=-1
		direcao.x=0
		velocidade=900
		$AnimatedSprite.set_frame(0)
		$TempoAtaque.stop()
	destruir()
	translate(velocidade*direcao*delta)
	if get_global_position().y>300 and vai_vem==false and vivo and PainelSingleton.get_parar_pista()==false:
		direcao=Vector2(-1,0)
		$AnimatedSprite.set_frame(0)
		velocidade=200
		vai_vem=true
		$TempoAtaque.start()
	if vai_vem and vivo:
		
		if get_global_position().x>600:
			direcao=Vector2(-1,0)
			if $AnimatedSprite.animation=="normal":
				$AnimatedSprite.set_frame(0)
		elif get_global_position().x<100:
			direcao=Vector2(1,0)
			if $AnimatedSprite.animation=="normal":
				$AnimatedSprite.set_frame(2)
			
func _on_TempoAtaque_timeout():
	
	if get_global_position().x<300:
		
		velocidade=0
		$AnimatedSprite.set_animation("Ataque_direito")
		$AnimatedSprite.playing=true
		
	elif get_global_position().x>300:
		velocidade=0
		$AnimatedSprite.set_animation("Ataque_esquerdo")
		$AnimatedSprite.playing=true


func _on_AnimatedSprite_animation_finished():
	$AnimatedSprite.stop()
	$AnimatedSprite.set_animation("normal")
	$AnimatedSprite.set_frame(1)
	velocidade=200
	


func _on_AnimatedSprite_frame_changed():
	if $AnimatedSprite.get_animation()=="Ataque_direito":
		if $AnimatedSprite.get_frame()==3:
			$DinamiteAnime.show()
		else:
			$DinamiteAnime.hide()
			if $AnimatedSprite.get_frame()==4:
				var din = dinamite.instance()
				din.set_global_position($Base_dinamite.get_global_position())
				get_parent().get_parent().add_child(din)
	elif $AnimatedSprite.get_animation()=="Ataque_esquerdo":
		if $AnimatedSprite.get_frame()==3:
			$DinamiteAnime2.show()
		else:
			$DinamiteAnime2.hide()
			if $AnimatedSprite.get_frame()==4:
				var din = dinamite.instance()
				din.set_global_position($Base_dinamite2.get_global_position())
				get_parent().get_parent().add_child(din)


func _on_MotoChefe_area_entered(area):
	if area.has_method("heroi"):
		$EfeitoCor.play("esbranquic")
		$Batida_heroi.play()
		$AnimeFim.play("Batida")
		vivo=false
		if area.get_global_position().x<get_global_position().x:
			set_global_position(get_global_position()+Vector2(100,0))
			direcao=Vector2(1,-0.3)
			velocidade=300
		else:
			set_global_position(get_global_position()+Vector2(-100,0))
			direcao=Vector2(-1,-0.3)
			velocidade=300
	elif area.has_method("choque"):
		vivo=false
		$EfeitoCor.play("esbranquic")
		$Choque.show()
		$Choque2.play()
		$AnimatedSprite.play("normal")
		$AnimeFim.play("BatidaChoque")
		if area.get_global_position().x<get_global_position().x:
			set_global_position(get_global_position()+Vector2(100,0))
			direcao=Vector2(1,-0.3)
			velocidade=300
		else:
			set_global_position(get_global_position()+Vector2(-100,0))
			direcao=Vector2(-1,-0.3)
			velocidade=300
		


func destruir():
	
	if get_global_position().y>=1500:
		PainelSingleton.reduzir_carros()
		PainelSingleton.pontuar(pontos)
		avisar_morte()
		get_parent().queue_free()		
	if get_global_position().x>1000:
		PainelSingleton.reduzir_carros()
		PainelSingleton.pontuar(pontos)
		avisar_morte()
		get_parent().queue_free()
	if get_global_position().x<-200:
		PainelSingleton.reduzir_carros()
		PainelSingleton.pontuar(pontos)
		avisar_morte()
		get_parent().queue_free()



func avisar_morte():
	if pai.has_method("chefe_moto_morreu"):
		pai.chefe_moto_morreu(true)
