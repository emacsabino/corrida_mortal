extends Area2D
var velocidade=400
func _ready():
	set_process(true)
	

	randomize()
	rotacionar_randomi()
	
func _process(delta):
	if PainelSingleton.get_movimentar_pista():
		velocidade=0
	get_parent().translate(velocidade*delta*Vector2(0,1))
	destruir()
	


func buraco_mar():
	pass
	
func rotacionar_randomi():
	var rot = rand_range(0,350)
	set_rotation(rot)
func destruir():
	
	if(get_global_position().y>=1400):
		queue_free()

func iniciar_animacao():
	velocidade=0
	$AnimationPlayer.play("caindo")
	

func _on_AnimationPlayer_animation_finished(anim_name):
	$Mergulho/CPUParticles2D.set_emitting(true)
