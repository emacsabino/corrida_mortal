extends Area2D
var direcao=Vector2(0,1)
var velocidade = 1000
var player
var dano = 10

func _ready():
	$Som.play()
	player = get_node("/root/Pista0/Carro/Corpo")
	#1.6-$Bala.rotation
	direcao.x=1/tan(rotation)
	#target_position = player.get_global_position().x
	#direcao.x = (target_position-get_global_position().x).normalized()
	if player.get_global_position().y<get_global_position().y:
		direcao.y=-1
		direcao.x = -direcao.x
func _process(delta):
	limpar_memoria()
	translate(direcao*velocidade*delta)
	
func set_rotacao(valor):
	rotate(valor)
func set_direcao(valor):
	direcao.x=valor
func limpar_memoria():
	if get_global_position().x>1000:
		get_parent().queue_free()
	elif get_global_position().x<0:
		get_parent().queue_free()
	if get_global_position().y>1500:
		get_parent().queue_free()
	elif get_global_position().y<-300:
		get_parent().queue_free()

func tiro_chefao():
	pass

func _on_corpo_area_entered(area):
	if area.has_method("heroi"):
		velocidade=0
		$AnimatedSprite.hide()
		$Impacto.show()
		$Impacto.playing=true

func danificar_heroi():
	return dano
func _on_Impacto_animation_finished():
	get_parent().queue_free()
