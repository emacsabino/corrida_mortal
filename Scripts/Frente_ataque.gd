extends Area2D
var dano=100
var batida_inimigo=0
var batida_gelo=0
var destroco = preload("res://Scenes/Armas_heroi/Destroco.tscn")
func _ready():
	
	pass # Replace with function body.

func criar_dano():
	return dano

func frente_pontas():
	pass
	
func set_ativar(valor:bool):
	set_collision_layer_bit(15,valor)
	set_collision_mask_bit(1,valor)

func _on_Timer_timeout():
	set_ativar(false)
	hide()
	is_visible_in_tree()

func _on_Frente_ataque_area_entered(area):
	if area.has_method("batida_corpo_inimigo"):
		
		batida_inimigo=batida_inimigo+1
		if batida_inimigo>=3:
			destrocar()
			set_ativar(false)
			hide()
			batida_inimigo=0
			
	if area.has_method("barreira"):
		
		batida_gelo=batida_gelo+1
		if batida_gelo>=7:
			set_ativar(false)
			hide()
		
func destrocar():
	var dest = destroco.instance()
	dest.set_position(get_position())
	get_parent().call_deferred("add_child",dest)
	dest.ativar()
	
func destruir_final():
	destrocar()
	set_ativar(false)
	hide()
