extends Area2D
var velocidade=300
var pontos=20
var armando = false
var atacando_moto = false
var direcao=Vector2(0,1)
var lateral = false
var normal = true
var bateu = false
var dano = 5
onready var player
var posicao_lado_esquerdo=false
var posicao_lado_direito = false
var posicao_centro=false
var b_atras=false
var b_left=false
var b_right=false
var b_frente=false
func _ready():
	
	player = get_node("/root/Pista0/Carro/Corpo")
	if posicao_lado_esquerdo:
		set_global_position(Vector2(150,0))
	if posicao_lado_direito:
		set_global_position(Vector2(600,0))
	if posicao_centro:
		set_global_position(Vector2(350,0))
		
	set_process(true)
	
func _process(delta):
	destruir()
	agir()
	movimentar_normal(delta)
	

func moto():
	pass
func danificar_carro():
	return dano
func movimentar_lateral(delta):
	if lateral:
		velocidade=180
		translate(velocidade*delta*direcao)
		if get_global_position().x<260:
			direcao.x=1
			direcao.y=0
			$Anime.set_frame(2)
			
		elif get_global_position().x>550:
			direcao.y=0
			direcao.x=-1
			$Anime.set_frame(0)
		
	
func movimentar_normal(delta):
	if normal:
		
		translate(velocidade*delta*direcao)
	
func escolher_posicao(esquerda:bool=false,centro:bool=false,direita:bool=false):
	posicao_lado_direito=direita
	posicao_centro=centro
	posicao_lado_esquerdo=esquerda
func agir():
	
	var dif_y = abs(get_global_position().y-player.get_global_position().y)
	var dif_x=abs(get_global_position().x-player.get_global_position().x)
	armando_moto(dif_x,dif_y)
	atacando(dif_x,dif_y)
	if armando and !bateu:
		desativar_ataque()
		atacando_moto=false
		if get_global_position().x< player.get_global_position().x:
			$Anime.set_animation("armando")
			$Anime.playing=true
		elif get_global_position().x>player.get_global_position().x:
			$Anime.set_animation("armando_left")
			$Anime.playing=true
	elif atacando_moto and !bateu:
		armando=false
		
		if get_global_position().x< player.get_global_position().x:
			$Anime.set_animation("atacando")
			$Anime.playing=true
			if $Anime.get_frame()==0:
				$Sons/Muchacho.play()
			if $Anime.get_frame()==2:
				
				ativar_ataque()
		elif get_global_position().x>player.get_global_position().x:
			$Anime.set_animation("atacando_left")
			$Anime.playing=true
			if $Anime.get_frame()==0:
				$Sons/Muchacho.play()
			if $Anime.get_frame()==2:
				ativar_ataque()
			
	elif !bateu:
		desativar_ataque()
		$Anime.set_animation("normal")
		$Anime.set_frame(1)
		
func atacando(dif_x,dif_y):
	if dif_x>10 and dif_x<250 and dif_y>=0 and dif_y<=150:
		atacando_moto=true
	else:
		atacando_moto=false
		
	
func armando_moto(dif_x,dif_y):
	if dif_x>250 and dif_x<350 and dif_y>=0 and dif_y<=150:
		armando=true
		$Desarmando.start()


func destruir():
	
	if get_global_position().y>=1500:
		PainelSingleton.reduzir_carros()
		PainelSingleton.pontuar(pontos)
		get_parent().queue_free()		
	if get_global_position().x>1000:
		PainelSingleton.reduzir_carros()
		PainelSingleton.pontuar(pontos)
		get_parent().queue_free()
	if get_global_position().x<-200:
		PainelSingleton.reduzir_carros()
		PainelSingleton.pontuar(pontos)
		get_parent().queue_free()

func _on_Desarmando_timeout():
	armando=false


func desativar_ataque():
	$Ataque_direito.set_collision_layer_bit(1,false)
	$Ataque_esquerdo.set_collision_layer_bit(1,false)
func ativar_ataque():
	$Ataque_direito.set_collision_layer_bit(1,true)
	$Ataque_esquerdo.set_collision_layer_bit(1,true)
	


		


func _on_Batidas_animation_finished(anim_name):
	
	if anim_name=="batida_atras":
		if b_atras:
			if posicao_lado_direito:
				$Anime.set_frame(2)
				direcao=Vector2(1,-0.3)
				velocidade=400
			elif posicao_lado_esquerdo:
				$Anime.set_frame(0)
				direcao=Vector2(-1,-0.3)
				velocidade=400
			elif posicao_centro:
				var fra = rand_range(0,2)
				var posic=[0,2]
				var escolha = int(fra)
				$Anime.set_frame(posic[escolha])
				if posic[escolha]==0:
					direcao=Vector2(-1,-0.3)
					velocidade=400
				else:
					direcao=Vector2(1,-0.3)
					velocidade=400
		elif b_right:
			$Anime.set_frame(0)
			direcao=Vector2(-1,-0.3)
			velocidade=400
		elif b_left:
			$Anime.set_frame(2)
			direcao=Vector2(1,-0.3)
			velocidade=400
		elif b_frente:
			if velocidade==200:
				$Anime.set_frame(2)
				direcao=Vector2(0,1)
				velocidade=400
			elif velocidade==0:
				if posicao_lado_direito:
					$Anime.set_frame(2)
					direcao=Vector2(1,-0.3)
					velocidade=400
				elif posicao_lado_esquerdo:
					$Anime.set_frame(0)
					direcao=Vector2(-1,-0.3)
					velocidade=400
				elif posicao_centro:
					var fra = rand_range(0,2)
					var posic=[0,2]
					var escolha = int(fra)
					$Anime.set_frame(posic[escolha])
					if posic[escolha]==0:
						direcao=Vector2(-1,-0.3)
						velocidade=400
					else:
						direcao=Vector2(1,-0.3)
						velocidade=400
				

func bateu_atras():
	esbranquicar()
	b_atras=true
	b_left=false
	b_right=false
	b_frente=false
	som_batida()
	bateu=true		
	velocidade=200
	direcao.y=-1
	position = position+Vector2(0,-100)
	$Batidas.play("batida_atras")
func batida_lado_direito():
	esbranquicar()
	b_atras=false
	b_left=false
	b_right=true
	b_frente=false
	som_batida()
	bateu=true		
	velocidade=200
	direcao.x=-1
	position = position+Vector2(-100,0)
	$Batidas.play("batida_atras")
func batida_lado_esquerdo():
	esbranquicar()
	b_atras=false
	b_left=true
	b_right=false
	b_frente=false
	som_batida()
	bateu=true		
	velocidade=200
	direcao.x=1
	position = position+Vector2(100,0)
	$Batidas.play("batida_atras")
func bateu_frente():
	esbranquicar()
	b_atras=false
	b_left=false
	b_right=false
	b_frente=true
	bateu=true		
	velocidade=0
	direcao.y=0
	som_batida()
	$Batidas.play("batida_atras")
func bateu_frente_em_carro():
	esbranquicar()
	b_atras=false
	b_left=false
	b_right=false
	b_frente=true
	bateu=true		
	velocidade=200
	direcao.y=1
	som_batida()
	$Batidas.play("batida_atras")
func som_batida():
	$Sons/Batida.play()
func esbranquicar():
	$Efeito_impacto.play("colisao")
