extends Node2D
var velocidade=300
var direcao = Vector2(0,1)
var cepo = preload("res://Scenes/pistas/Inimigos/Cepo_espinho.tscn")
var entrada = true
var parti = preload("res://Scenes/particulas/batida_chefe.tscn")
var zigzag = false
var batida = false
var pontos=100
var expl = preload("res://Scenes/Explosao_G.tscn")
var life = 600
var bateu_ferro=false


func _ready():
	add_to_group("inimigos")
	$Area2D/Time_iniciar_cepo.start()

func _process(delta):
	destruir()
	batida_ferro(delta)
	morrer(delta)
	if PainelSingleton.get_parar_pista_total():
			batida = false
			zigzag = false
			entrada = false
	if PainelSingleton.get_movimentar_pista() and life>0:
		direcao.y=-1
		velocidade=2000
		direcao.x=0
		zigzag=false
		translate(direcao*delta*velocidade)
	if batida:
		translate(velocidade*delta*Vector2(0,-1))
	else:
		entrad(delta)
		zig_zag(delta)
	
		
func entrad(del):
	if entrada:
		translate(direcao*velocidade*del)
		if get_global_position().y>=390:
			direcao.x=0.4
func zig_zag(del):
	if get_global_position().y>=400:
		entrada=false
		zigzag=true
		
	if zigzag:
		if get_global_position().y>=400:
			direcao.y=-0.4
		elif get_global_position().y<=100:
			direcao.y=0.4
		
		if get_global_position().x>=550:
			direcao.x=-0.4
		elif get_global_position().x<=200:
			direcao.x=0.4
		translate(direcao*velocidade*del)

func _on_Area2D_area_entered(area):
	if area.has_method("heroi"):
		$Batida.play()
		levou_batida()
		reduzir_energia(5)
		if area.get_global_position().x<get_global_position().x:
			set_global_position(get_global_position()+Vector2(20,0))
		elif area.get_global_position().x>get_global_position().x:
			set_global_position(get_global_position()+Vector2(-20,0))
		if area.get_global_position().y<get_global_position().y:
			set_global_position(get_global_position()+Vector2(0,20))
		elif area.get_global_position().x>get_global_position().x:
			set_global_position(get_global_position()+Vector2(0,-20))
	if area.has_method("frente_pontas"):
		
		var brilh = parti.instance()
		var pos = to_local(area.get_global_position())
		add_child(brilh)
		brilh.set_position(pos)
		brilh.get_node("CPUParticles2D").emitting=true
		$Estrondo.play()
		reduzir_energia(area.criar_dano())
		$Area2D/Esbranquicar.play("esbranqui")
		set_global_position(Vector2(get_global_position().x,get_global_position().y-50))
		bateu_ferro=true
		zigzag=false
		$Time_fim_bat_ferro.start()
	if area.has_method("missel"):
		
		explosao_total()

func reduzir_energia(valor):
	life = life - valor


func levou_batida():
	if !PainelSingleton.get_movimentar_pista():
		batida=true
		$Area2D/Timer.start()
	

func _on_Timer_timeout():
	batida=false
func gerar_cepos():
	var cp = cepo.instance()
	cp.set_global_position($Area2D/buraco.get_global_position())
	get_parent().add_child(cp)
	cp.player_anime()

func _on_Time_cepo_timeout():
	gerar_cepos()


func _on_Time_iniciar_cepo_timeout():
	$Area2D/Time_cepo.start()


func _on_Time_destruir_timeout():
	queue_free()

func destruir():
	
	if get_global_position().y>=1500:
		PainelSingleton.reduzir_carros()
		PainelSingleton.pontuar(pontos)
		queue_free()		
	if get_global_position().x>1000:
		PainelSingleton.reduzir_carros()
		PainelSingleton.pontuar(pontos)
		queue_free()
	if get_global_position().x<-200:
		PainelSingleton.reduzir_carros()
		PainelSingleton.pontuar(pontos)
		queue_free()
		
func morrer(delta):
	if life<=0:
		$Area2D/CollisionPolygon2D.set_deferred("disabled",true)
		
		zigzag=false
		if get_global_position().x<300:
			direcao=Vector2(-0.6,-0.5)
		else:
			direcao=Vector2(0.6,-0.5)
		translate(delta*velocidade*direcao)
func batida_ferro(delta):
	if bateu_ferro:
		translate(velocidade*Vector2(0,-1)*delta)
	
		
		

func _on_Time_fim_bat_ferro_timeout():
	bateu_ferro=false
	zigzag=true
	
func explosao_total():
	var explodir = expl.instance()
	explodir.set_dimensao(2,2)
	explodir.set_ativo(true)
	velocidade=0	
	explodir.set_global_position(get_global_position())
	get_parent().add_child(explodir)
	PainelSingleton.reduzir_caminhao()
	$Area2D/CollisionPolygon2D.set_deferred("disabled",true)
	explodir.get_node("AnimatedSprite").playing=true
	$Area2D/buraco.hide()
	$Area2D/Time_cepo.stop()
	$Area2D/Sprite.hide()
	$Som_explosao.play()
	$Area2D/Som_motor.stop()
	
	$Time_destruir.start()
