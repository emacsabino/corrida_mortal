extends Area2D

var velocidade=200


func _ready():
	set_process(true)

func _process(delta):
	translate(Vector2(0,1)*velocidade*delta)
	if get_global_position().y>=2577.303:
		set_global_position(Vector2(354.736,-1966.549))

func reduzir_velocidade(valor):
	velocidade=valor
