extends Node2D
var velocidade=280
var direcao = Vector2(0,1)
var dano
var particula = preload("res://Scenes/particulas/batida_chefe.tscn")
var movimentar=false
func _ready():
	$Area2D/Jogou.play()

func _process(delta):
	if movimentar:
		translate(velocidade*delta*direcao)
	destruir()

	
func danificar_heroi():
	return dano
func player_anime():
	$Area2D/Anime_zoom.play("zoom")

func _on_Anime_zoom_animation_finished(_anim_name):
	movimentar=true

func destruir():
	
	if get_global_position().y>=1500:
		
		
		queue_free()		
	if get_global_position().x>1000:
		
		
		queue_free()
	if get_global_position().x<-200:
		
		
		queue_free()


func _on_Area2D_area_entered(area):
	if area.has_method("heroi"):
		
		
		var brilho = particula.instance()
		brilho.escalar(2.0,2.0)
		brilho.set_position(to_local($Area2D/AnimatedSprite.get_global_position()))
		$Area2D/AnimatedSprite.hide()
		add_child(brilho)
		$Cepo.play()
		
		


func _on_Cepo_finished():
	queue_free()
