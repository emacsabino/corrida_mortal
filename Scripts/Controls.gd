extends Node2D

func _ready():
	pass


func _on_Lancar_missil_pressed():
	PainelSingleton.set_lancar_missile(true)


func _on_pause_pressed():
	$pause.hide()
	$play.show()
	PainelSingleton.texto_pause(true)
	get_tree().paused=true


func _on_play_pressed():
	$pause.show()
	$play.hide()
	PainelSingleton.texto_pause(false)
	get_tree().paused=false
	
