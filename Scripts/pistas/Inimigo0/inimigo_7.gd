extends Area2D
var velocidade = 1200
var direcao = Vector2(0,1)
var relogio= true
var explosao_g = preload("res://Scenes/Explosao_G.tscn")
func _ready():
	add_to_group("inimigos")
	$Motor.play()
	
	
	
func _process(delta):
	destruir()
	
	if $Node2D/Bloco.bater_bloco() and relogio:
		
		$Anime.play("tremor")
		velocidade=20
		direcao.y=-1
		$Tempo_parada.start()
		relogio=false
	translate(velocidade*delta*direcao)
	
	


func _on_Tempo_parada_timeout():
	velocidade=0
func destruir():
	if get_global_position().y>=1500:
		PainelSingleton.reduzir_carros()
		
		get_parent().queue_free()		
	if get_global_position().x>1000:
		PainelSingleton.reduzir_carros()
	
		get_parent().queue_free()
	if get_global_position().x<-200:
		PainelSingleton.reduzir_carros()
		
		get_parent().queue_free()

func _on_Area2D_area_entered(area):
	if area.has_method("missel"):
		explosao_total()
		
func explosao_total():
	var explodir = explosao_g.instance()
	add_child(explodir)
	$Node2D.hide()
	$Node2D/Bloco/CollisionPolygon2D.set_deferred("disabled",true)
	explodir.set_position(to_local(get_global_position()))
	explodir.get_node("AnimatedSprite").playing=true
	$Sprite.hide()
	$CollisionPolygon2D.set_deferred("disabled",true)
