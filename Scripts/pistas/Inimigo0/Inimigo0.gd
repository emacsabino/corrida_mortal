extends Area2D
var velocidade = 300
var direcao = Vector2(0,1)
var faixa_esquerda = true
var pontos=10
var dano = 10
var carro_escolha
var conta_batida=0
func _ready():
	randomize()
	add_to_group("carros_inimigos")
	var n = rand_range(0,5)
	var escol = int(n)
	carro_escolha=escol
	var image = load("res://Assets/pistas/pista0/inimigos/gipe"+str(escol)+".png")
	$Ini.set_deferred("texture",image)
	set_process(true)
func _process(delta):
	translate(velocidade*delta*direcao)
	if PainelSingleton.get_movimentar_pista():
			direcao.y=-1
			velocidade=500
	destruir()	
	

func faixa_esquerda(valor:bool):
	faixa_esquerda=valor

func _on_Corpo_area_entered(area):
	if area.has_method("heroi"):
		conta_batida=conta_batida+1
		$batida.play()
		if carro_escolha==3:
			$Fumacas_fusca.show()
		else:
			$Fumacas_gipe.show()
			
		danificar_heroi()
		$Efeito_Cor_Anime.play("esbranquicar")
		set_global_position(Vector2(get_global_position().x,get_global_position().y-100))
		if conta_batida>1:
			descontrolar(area)
				
		
	
func danificar_heroi():
	PainelSingleton.reduzir_energia(dano)	
func batida_corpo_inimigo():
	pass	
func destruir():
	
	if get_global_position().y>=1500:
		PainelSingleton.reduzir_carros()
		PainelSingleton.pontuar(pontos)
		queue_free()		
	if get_global_position().x>1000:
		PainelSingleton.reduzir_carros()
		PainelSingleton.pontuar(pontos)
		queue_free()
	if get_global_position().x<-200:
		PainelSingleton.reduzir_carros()
		PainelSingleton.pontuar(pontos)
		queue_free()
func descontrolar(area):
	
		$Batida01.play("descontrole")
		if faixa_esquerda:
			if area.get_global_position().y<get_global_position().y:
				direcao=Vector2(-1.2,1.4)
			else:
				direcao=Vector2(-1.2,-0.5)
		else:
			if area.get_global_position().y<get_global_position().y:
				direcao=Vector2(1.2,1.4)
			else:
				direcao=Vector2(1.2,-0.5)
