extends Node2D
var buraco = preload("res://Scenes/Buraco_mar.tscn")

func _ready():
	PainelSingleton.exibir_painel()
	$PrimeiraP.play("introducao")
	randomize()

func _on_Timer_timeout():
	var bur = buraco.instance()
	
	var posi_x = rand_range(240,472)
	bur.set_global_position(Vector2(posi_x,-300))
	
	add_child(bur)

	


func _on_PrimeiraP_animation_finished(anim_name):
	if anim_name=="introducao":
		$Ponte.show()
		$Ponte2.show()
		$Timer.start()
		$PrimeiraP.play("PrimeiraP")
		

func _on_Abertura_animation_finished(anim_name):
	$Carro/Corpo.operar_carro()
