extends Node2D
var inim_fusca = preload("res://Scenes/pistas/Inimigos/Inimigo_fusca.tscn")
var inim = preload("res://Scenes/pistas/Inimigos/Inimigo4.tscn")
var sinal_rap = preload("res://Scenes/powers/sinal_rapidez.tscn")
var sinal_lif = preload("res://Scenes/powers/Sinal_life.tscn")
var sinal_choque = preload("res://Scenes/powers/sinal_raio.tscn")
var sinal_missel = preload("res://Scenes/powers/Sinal_missel.tscn")
var gerar_inimigos = false
var gerar_sinal_rapidez=false
var gerar_sinal_life = true
var estah_direita=false
var gerar_sinal_choque= true
var contador=0
var gerar_choque_qtd_carros=true
var caminhao = preload("res://Scenes/pistas/Inimigos/Caminhao.tscn")
var gerar_caminhao = true

func _ready():
	
	salvar_fase_atual("Pista0")
	PainelSingleton.carregar_qtd_carros(25) #25
	PainelSingleton.carregar_qtd_caminhao(1)
	PainelSingleton.exibir_painel()
	randomize()


func _on_Gerar_inimigos_timeout():
	if PainelSingleton.retornar_qtd_carros()<10 and gerar_choque_qtd_carros:
		gerar_sinal_choque=true
		gerar_choque_qtd_carros=false
	if gerar_sinal_choque:
		var choq = sinal_choque.instance()
		choq.set_global_position(Vector2(350,-500))
		add_child(choq)
		gerar_sinal_choque=false
	if gerar_sinal_life:
		if PainelSingleton.retornar_energia_heroi()<=50:
			var life =sinal_lif.instance()
			life.set_global_position(Vector2(350,-500))
			life.get_node("sinal_life").set_velocidade(600)
			add_child(life)
			gerar_sinal_life=false
		
	if gerar_sinal_rapidez:
		if PainelSingleton.retornar_qtd_carros()<=12:
			var rapidez =sinal_rap.instance()
			rapidez.set_global_position(Vector2(250,-500))
			rapidez.get_node("sinal_rapidez").set_velocidade(600)
			add_child(rapidez)
			gerar_sinal_rapidez=false
	if len(get_tree().get_nodes_in_group("inimigos"))<2 and gerar_inimigos and PainelSingleton.estah_vivo():
		if PainelSingleton.retornar_qtd_carros()>0:
			var tip_carr = int(rand_range(0,2))
			var inimig
			if tip_carr==0:
				inimig = inim_fusca.instance()
			else:
				inimig = inim.instance()
			var list = [Vector2(270,-500),Vector2(500,-500)]
			var escolha = int(rand_range(0,2))
			
			if escolha==1 and estah_direita==false:
				inimig.get_node("Corpo").faixa_esquerda(false)
				estah_direita=true
			else:
				escolha=0
				if estah_direita==true:
					estah_direita=false
				else:
					escolha=1
					estah_direita=true
					inimig.get_node("Corpo").faixa_esquerda(false)
						
				
			
					 
			inimig.set_global_position(list[escolha])
		
			
			add_child(inimig)
			
		else:
			if gerar_caminhao:
				var list = [Vector2(270,-500),Vector2(500,-500)]
				var escolha = int(rand_range(0,2))
				var cam = caminhao.instance()
				cam.set_global_position(list[escolha])
				add_child(cam)
				gerar_caminhao=false
				$Gerar_sinal_missel.start()
				$Time_ir_fase_seguinte.start()
			
			#$Finalizacao.play("Mudar_Fase")


func _on_Abertura_animation_finished(_anim_name):
	$Carro/Corpo.operar_carro()
	gerar_inimigos=true
	
func ir_para_fase_seguinte():
	if get_tree().change_scene("res://Scenes/pistas/Pista01.tscn")!=OK:
		print ("An unexpected error occured when trying to switch to the Readme scene")

func salvar_fase_atual(pista:String):
	var file = File.new()
	file.open("user://save_game.dat",file.WRITE)
	file.store_string(pista)
	file.close()
func desativar_fundo_musical():
	$AudioStreamPlayer2D.stream_paused=true
func ativar_fundo_musical():
	$AudioStreamPlayer2D.stream_paused=false

func _on_Som_ativo_pressed():
	desativar_fundo_musical()
	$Som_ativo.hide()
	$Som_inativo.show()


func _on_Som_inativo_pressed():
	ativar_fundo_musical()
	$Som_ativo.show()
	$Som_inativo.hide()


func _on_Gerar_sinal_missel_timeout():
	if contador<1:
		var missel = sinal_missel.instance()
		missel.set_global_position(Vector2(300,-300))
		add_child(missel)


func _on_Time_ir_fase_seguinte_timeout():
	if contador==1:
		$Finalizacao.play("Mudar_Fase")
	if PainelSingleton.get_qtd_caminhao()==0:
		
		contador=contador+1
		
	
