extends Node2D

var ativo
var velocidade=400
var direcao=Vector2(0,1)
func _ready():
	$Explosao.play()
func _process(delta):
	if ativo:
		translate(direcao*velocidade*delta)
	
func set_velocidade(vel):
	velocidade=vel
func set_ativo(valor):
	ativo=valor
func set_direcao(valor:Vector2):
	direcao=valor

func _on_Explosao_finished():
	queue_free()
func set_dimensao(x,y):
	set_scale(Vector2(x,y))
	
