extends Area2D

func _ready():
	desativar()
	
func buraco_fundo():
	pass

func desativar():
	get_parent().get_node("Rampa/Rampa").desativar()
	set_collision_mask_bit(14,false)
	
func ativar():
	get_parent().get_node("Rampa/Rampa").ativar()
	set_collision_mask_bit(14,true)
	




func _on_Buraco_area_entered(area):
	if area.has_method("heroi"):
		get_node("Carro/Corpo").ativar_frente()
		get_node("Carro/Corpo").set_global_position(area.get_global_position())
		$Carro.show()
		get_node("Carro/Corpo").caindo_buraco_fundo()
		
