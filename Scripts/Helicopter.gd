extends Node2D
var tiro = preload("res://Scenes/Tiro_helicopter.tscn")
var velocidade=200
var direcao=Vector2(0,1)
func _ready():
	randomize()
	movimento2()
	$Corpo/Helice_som.play()
	$Time_iniciar_tiros.start()
	
func _process(delta):
	translate(velocidade*direcao*delta)
	if get_global_position().y>1000.0:
		$Corpo/Metralhar.stop()
		$Corpo/Tiro1.hide()

func _on_Timer_tiro_timeout():
	$Corpo/Metralhar.play()
	var tir = tiro.instance()
	var posicao_x = rand_range(-100,100)
	var posicao_y = rand_range(550,650)
	tir.set_global_position(get_global_position()+Vector2(posicao_x,posicao_y))
	get_parent().add_child(tir)

func movimento2():
	direcao.x=-0.4
	$Timer_variacao_x.start()
func movimento3():
	velocidade=velocidade+100
	

func _on_Timer_variacao_x_timeout():
	
	direcao.x=-direcao.x


func _on_Time_iniciar_tiros_timeout():
	$Corpo/Tiro1.show()
	$Corpo/Timer_tiro.start()
