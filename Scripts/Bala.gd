extends Node2D
var velocidade = 1000
var direcao = Vector2(0,1)
var ativar_tiro = true

func _ready():
	
	set_process(true)
	

func _process(delta):
	if ativar_tiro:
		var dife = 1.6-$Bala.rotation
		$Bala.translate(Vector2(dife,1)*velocidade*delta)
	limpar_memoria()
	
		
func set_rotacao(valor):
	$Bala.rotation=valor
	

func set_direcao(valor:Vector2):
	direcao=valor



func _on_Bala_area_entered(area):
	if area.has_method("heroi"):
		$Bala.danificar_heroi()
		queue_free()
		
func limpar_memoria():
	if get_global_position().x>1000:
		queue_free()
	elif get_global_position().x<0:
		queue_free()
	if get_global_position().y>1500:
		queue_free()
	elif get_global_position().y<-300:
		queue_free()
