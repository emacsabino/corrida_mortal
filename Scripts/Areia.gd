extends Node2D

export var angulo=20
export var color:Color
export var speed:float


func _ready():
	$CPUParticles2D.set_rotation(deg2rad(angulo))
	$CPUParticles2D.set_color(color)
	$CPUParticles2D.set_speed_scale(speed)
	


