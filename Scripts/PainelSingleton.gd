extends Node2D
var par_left
var par_right
var vivo:bool=true
var stop_pista=false
var stop_pista2 = false
var parar_pista_total_retorn = false
var gerar_inimigos = true


var qtd_caminhao=0

onready var energia_carro = $CanvasLayer/ColorRect/TextureProgress
onready var qtd_carros = $CanvasLayer/ColorRect/Carros
onready var pontos = $CanvasLayer/ColorRect/Label/Pontos

func _ready():
	ganhar_energia(100)
	pass # Replace with function body.

func get_position_parede_esq():
	return par_left
func get_position_parede_dir():
	return par_right
func parar_pista2():
	stop_pista2=true
func parar_pista_total():
	parar_pista_total_retorn=true
	stop_pista=true
func parar_pista():
	stop_pista=true
	exibir_game_over()
func get_movimentar_pista():
	return stop_pista
	

func get_parar_pista2():
	return stop_pista2
func get_parar_pista_total():
	return parar_pista_total_retorn
#--------------------
#posicao carro esquerdo

	

	

		

func reduzir_energia(valor):
	energia_carro.value=energia_carro.value-valor
	verificar_status_carro()
	
		
func ganhar_energia(valor):
	energia_carro.value=energia_carro.value+valor
	verificar_status_carro()
func energia_maxima(valor):
	energia_carro.set("max_value",valor)
	verificar_status_carro()
func reduzir_carros():
	var qt =int(qtd_carros.get_text())
	if qt>=1 and !stop_pista:
		qt = qt -1
		qtd_carros.set_text(str(qt))
func retornar_energia_heroi():
	return int(energia_carro.value)
func verificar_status_carro():
	var energia = retornar_energia_heroi()
	if energia>60 and energia<=100:
		$CanvasLayer/ColorRect/Sprite/Stable.show()
		$CanvasLayer/ColorRect/Sprite/Damaged.hide()
		$CanvasLayer/ColorRect/Sprite/Critical.hide()
	elif energia>20 and energia<=60:
		$CanvasLayer/ColorRect/Sprite/Stable.hide()
		$CanvasLayer/ColorRect/Sprite/Damaged.show()
		$CanvasLayer/ColorRect/Sprite/Critical.hide()
	elif energia<=20:
		$CanvasLayer/ColorRect/Sprite/Stable.hide()
		$CanvasLayer/ColorRect/Sprite/Damaged.hide()
		$CanvasLayer/ColorRect/Sprite/Critical.show()
		
func carregar_qtd_carros(valor):
	qtd_carros.set_text(str(valor))
func carregar_qtd_caminhao(valor):
	qtd_caminhao=valor
func reduzir_caminhao():
	qtd_caminhao=qtd_caminhao-1
func get_qtd_caminhao():
	return qtd_caminhao
func retornar_qtd_carros():
	return int(qtd_carros.get_text())
	
func pontuar(valor):
	var p = int(pontos.get_text())
	p = p+valor
	pontos.set_text(str(p))
func exibir_game_over():
	$CanvasLayer/Time_game_over.start()
func exibir_painel():
	$CanvasLayer/ColorRect.show()
func get_parar_pista():
	return stop_pista
func estah_vivo():
	return vivo
func set_vivo(valor:bool):
	vivo=valor
func texto_pause(valor:bool):
	if valor:
		$CanvasLayer/Pause.show()
	else:
		$CanvasLayer/Pause.hide()

func exibir_continue():
	$CanvasLayer/Time_continue.start()




func _on_Continue_pressed():
	if get_tree().reload_current_scene()!=OK:
		print ("An unexpected error occured when trying to switch to the Readme scene")
	
	$CanvasLayer/Continue.hide()
	set_vivo(true)
	stop_pista=false
	$CanvasLayer/Game_over.hide()
	$CanvasLayer/Restart.hide()
	ganhar_energia(energia_carro.max_value)
	zerar_pontuacao()
	
	


func _on_Restart_pressed():
	if get_tree().change_scene("res://Scenes/Capa.tscn")!=OK:
		print ("An unexpected error occured when trying to switch to the Readme scene")
	$CanvasLayer/Continue.hide()
	set_vivo(true)
	stop_pista=false
	$CanvasLayer/Game_over.hide()
	$CanvasLayer/Restart.hide()
	ganhar_energia(energia_carro.max_value)
	$CanvasLayer/ColorRect.hide()
	zerar_pontuacao()
	
func zerar_pontuacao():
	pontos.set_text(str(0))
func reiniciar_jogo_na_fase_final():
	if get_tree().change_scene("res://Scenes/Capa.tscn")!=OK:
		print ("An unexpected error occured when trying to switch to the Readme scene")
	$CanvasLayer/Continue.hide()
	set_vivo(true)
	stop_pista=false
	$CanvasLayer/Game_over.hide()
	$CanvasLayer/Restart.hide()
	ganhar_energia(energia_carro.max_value)
	$CanvasLayer/ColorRect.hide()
	zerar_pontuacao()


func _on_Time_continue_timeout():
	$CanvasLayer/Continue.show()
	$CanvasLayer/Restart.show()


func _on_Time_game_over_timeout():
	$CanvasLayer/Game_over.show()
