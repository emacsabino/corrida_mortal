extends Node2D

var velocidade = 200
var direcao = Vector2(0,1)
var pontos=200

var particulas = preload("res://Scenes/particulas/Explosao_eletrica.tscn")
var explosao_g = preload("res://Scenes/Explosao_G.tscn")
func _ready():
	add_to_group("inimigos")

func _process(delta):
	translate(velocidade*direcao*delta)
	if PainelSingleton.get_movimentar_pista():
			direcao.y=-1
			velocidade=700
	if PainelSingleton.retornar_qtd_carros()<=0:
		direcao.y=0
		direcao.x=1
	destruir()	


func _on_Area2D_area_entered(area):
	if area.has_method("heroi"):
		set_global_position(Vector2(get_global_position().x,get_global_position().y-100))
		$Area2D/Batida.play()
		$Anime_esbranquicar.play("esbranquicar")
		$Anime_tremor.play("tremor")
		direcao.y=-1
		$Voltar_descer.start()
	if area.has_method("choque"):
		set_global_position(Vector2(get_global_position().x,get_global_position().y-100))
		direcao=Vector2(1.2,-1.4)
		
		$Anime_tremor.play("tremor")
		$Choque/Choque.play()
		$Choque.show()
		$Particulas_time.start()
	if area.has_method("missel"):
		explosao_total()
		
	if area.has_method("bazuca"):
		explosao_total()
	if area.has_method("frente_pontas"):
		receber_frente_pontas()
func receber_frente_pontas():
	$Area2D/Batida.play()
	set_global_position(Vector2(get_global_position().x,get_global_position().y-100))
	$Anime_esbranquicar.play("esbranquicar")
	direcao=Vector2(1.2,-1.4)
	$Anime_tremor.play("tremor_loop")
func explosao_total():
	var explodir = explosao_g.instance()
	explodir.set_global_position(get_global_position())
	explodir.set_dimensao(2,2)
	explodir.set_velocidade(velocidade)
	explodir.set_direcao(direcao)
	explodir.set_ativo(true)
	get_parent().get_parent().add_child(explodir)
	explodir.get_node("AnimatedSprite").playing=true
	queue_free()
func _on_Voltar_descer_timeout():
	direcao.y=direcao.y+0.25
	if direcao.y==1:
		$Voltar_descer.stop()
func destruir():
	
	if get_global_position().y>=2300:
		PainelSingleton.reduzir_carros()
		PainelSingleton.pontuar(pontos)
		queue_free()		
	if get_global_position().x>1500:
		PainelSingleton.reduzir_carros()
		PainelSingleton.pontuar(pontos)
		queue_free()
	if get_global_position().x<-500:
		PainelSingleton.reduzir_carros()
		PainelSingleton.pontuar(pontos)
		queue_free()


func _on_Particulas_time_timeout():
	var part = particulas.instance()
	var posi_x = rand_range(-54,48)
	var posi_y = rand_range(-121,60)
	add_child(part)
	part.set_position(Vector2(posi_x,posi_y))

func set_velocidade(valor):
	velocidade=valor
