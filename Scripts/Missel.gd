extends Area2D
var direcao_y=Vector2(0,-1)
var direcao_x=Vector2(0,-1)
var velocidade_x=200
var velocidade_y=700
export var atacar = false
var atacar_right=false
var atacar_left=false
var rotacionar = true
var movimento_x=true

var tem_alvo=true

func _ready():
	if PainelSingleton.escolheu_alvo_missel==false:
		tem_alvo=false
	set_process(true)
	get_parent().get_node("Rotacionar").start()
	$Movimento.play("partida")

func _process(delta):
	if tem_alvo:
	
		translate(velocidade_x*direcao_x*delta)
		translate(velocidade_y*direcao_y*delta)
		destruir_fora_da_tela()
		controlar_movimento_x()
	else:
		translate(1600*Vector2(0,-1)*delta)
	

func missel():
	pass
func girar():
	$AnimatedSprite.play()
func acender_turbina():
	$CPUParticles2D2.show()
func set_atacar(valor:bool):
	atacar=valor
func escolher_inimigo_right(right:bool):
	
	if right:
		atacar_right=true
	else:
		atacar_left=true
		
func perseguir_inimigo():
	
	
	if atacar_right and PainelSingleton.get_posicao_inimigo_right()!=null:
		
		var posicao_inimig=PainelSingleton.get_posicao_inimigo_right()
	
		if get_global_position().x<=posicao_inimig.x:
			direcao_x.x=1
				
		else:
			direcao_x.x=-1		
			
		if get_global_position().y>=posicao_inimig.y:
			direcao_y.y=-1
		else:
			direcao_y.y=1
			
	elif atacar_left and PainelSingleton.get_posicao_inimigo_left()!=null:
		
		var posicao_inimig=PainelSingleton.get_posicao_inimigo_left()
		
		if get_global_position().x<=posicao_inimig.x:
			direcao_x.x=1
		else:
			direcao_x.x=-1
		
		if get_global_position().y>=posicao_inimig.y:
			direcao_y.y=-1
		else:
			direcao_y.y=1
	
		
		
		

func _on_Timer_timeout():
	if tem_alvo:
		velocidade_y=velocidade_y+400
		if movimento_x:
			velocidade_x = velocidade_x+300
		perseguir_inimigo()
	
func rotacionar_missel():
	if tem_alvo:
		if rotacionar and atacar_right:
			var p_inim=PainelSingleton.get_posicao_inimigo_right()
			var p_misse=get_global_position()
			var dif_x
			var dif_y
			if p_inim!=null:
				dif_x = p_misse.x-p_inim.x
				dif_y = p_misse.y-p_inim.y
				if dif_x>95 and dif_x<259:
					if (dif_y>309 and dif_y<760):
						$Movimento.play("virada_esquerda_cima")
					elif (dif_y>-110 and dif_y<160):
						$Movimento.play("virada_esquerda")
					elif (dif_y>-1000 and dif_y<-240):
						$Movimento.play("virada_esquerda_baixo")
				elif dif_x>-259 and dif_x<-95:
					if (dif_y>309 and dif_y<760):
						$Movimento.play("virada_direita_cima")
					elif (dif_y>-110 and dif_y<160):
						$Movimento.play("virada_direita")
					elif (dif_y>-1000 and dif_y<-240):
						$Movimento.play("virada_direita_baixo")
				elif dif_x>-95 and dif_x<95:
					if dif_y>0:
						$Movimento.play("cima")
					elif dif_y<0:
						$Movimento.play("baixo")
			
		if rotacionar and atacar_left:
			var p_inim=PainelSingleton.get_posicao_inimigo_left()
			var p_misse=get_global_position()
			var dif_x
			var dif_y
			if p_inim!=null:
				dif_x = p_misse.x-p_inim.x
				dif_y = p_misse.y-p_inim.y
				if dif_x>95 and dif_x<259:
					if (dif_y>309 and dif_y<760):
						$Movimento.play("virada_esquerda_cima")
					elif (dif_y>-110 and dif_y<160):
						$Movimento.play("virada_esquerda")
					elif (dif_y>-1000 and dif_y<-240):
						$Movimento.play("virada_esquerda_baixo")
				elif dif_x>-259 and dif_x<-95:
					if (dif_y>309 and dif_y<760):
						$Movimento.play("virada_direita_cima")
					elif (dif_y>-110 and dif_y<160):
						$Movimento.play("virada_direita")
					elif (dif_y>-1000 and dif_y<-240):
						$Movimento.play("virada_direita_baixo")
				elif dif_x>-95 and dif_x<95:
					if dif_y>0:
						$Movimento.play("cima")
					elif dif_y<0:
						$Movimento.play("baixo")
		
			
		
	#rotacionar=false

	

func _on_Rotacionar_timeout():
	
	rotacionar_missel()


func _on_Missel_area_entered(_area):
	queue_free()

func controlar_movimento_x():
	var posicao_inimig_l=PainelSingleton.get_posicao_inimigo_left()
	var posicao_inimig_r = PainelSingleton.get_posicao_inimigo_right()
	if posicao_inimig_l!=null and atacar_left:
		if abs(get_global_position().x-posicao_inimig_l.x)<20:
			velocidade_x=100
			movimento_x=false
	
	if posicao_inimig_r!=null and atacar_right:
		if abs(get_global_position().x-posicao_inimig_r.x)<20:
			velocidade_x=100
			movimento_x=false

func _on_Movimento_animation_finished(anim_name):
	if anim_name=="partida":
		atacar=false

func destruir_fora_da_tela():
	if get_global_position().y>=1300:
		
		get_parent().queue_free()
	if get_global_position().y<-1200:
		
		get_parent().queue_free()
	if get_global_position().x>1000:
		
		get_parent().queue_free()
	if get_global_position().x<-200:
		
		get_parent().queue_free()
