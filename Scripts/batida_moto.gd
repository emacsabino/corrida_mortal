extends Area2D
var bat = preload("res://Scenes/particulas/brilho_moto_batida.tscn")
var dano_ao_carro=5

func _on_batida_atras_area_entered(area):
	if area.has_method("heroi") or area.has_method("moto"):
		
		var impacto = bat.instance()
		add_child(impacto)
		impacto.play("brilho_impacto")
		get_parent().bateu_atras()
	
func moto():
	pass
func danificar_carro():
	return dano_ao_carro
