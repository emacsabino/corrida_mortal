extends Area2D
var velocidade = 300
var movimenta=true
var contador_explosoes=0
var anime_missel=0
var contador_missel = 0
var pipoco = preload("res://Scenes/Explosao2.tscn")
var fuma = preload("res://Scenes/Fumaca.tscn")
var operando=false
var destruir_pontas = true
var batid_chefao = false
var pegou_power =false
var faisca_batida = preload("res://Scenes/particulas/batida_chefe.tscn")
var missel2 = preload("res://Scenes/Armas_heroi/Missel _atual.tscn")
var limite_direito=667
var limite_esquerdo=66
var posicao_control_original
var posicao_cabeca_original
var mover_control_pai=true
var seta_up
var seta_right
var seta_down
var seta_left
var explosao_g = preload("res://Scenes/Explosao_G.tscn")
#----direcao-------
var right=false
var left = false
var down = false
var up = false

#---toques_missel----
var toque=0

func _ready():
	seta_up=get_parent().get_node("Control/Seta_up").get_position()
	seta_right=get_parent().get_node("Control/Seta_right").get_position()
	seta_down = get_parent().get_node("Control/Seta_down").get_position()
	seta_left = get_parent().get_node("Control/Seta_left").get_position()
	posicao_control_original=get_parent().get_node("Control").get_position()
	posicao_cabeca_original = get_parent().get_node("Control/Seta_cabeca").get_position()
	set_process(true)
	set_process_input(true)
	ocultar_lancador_com_missel()
	$Sons/motor.play()
	

func _process(delta):
	
	movimentar_carro_final(delta)
	parar_controle_carro()
	
		
	
	if operando:
		movimentacao2(delta)
	
	#----controle para o carro não sair da tela
	if(get_global_position().y>1080):
		set_global_position(Vector2(get_global_position().x,1080))
	if(get_global_position().y<=143):
		set_global_position(Vector2(get_global_position().x,143))
	if(get_global_position().x<=limite_esquerdo):
		set_global_position(Vector2(limite_esquerdo,get_global_position().y))
	if(get_global_position().x>=limite_direito):
		set_global_position(Vector2(limite_direito,get_global_position().y))
	#--------------------------------------------------------------------------
	if batid_chefao:
		translate(velocidade*delta*Vector2(0,1))
	
#-----implementação para controlar o carro com o direcional virtual------
func _input(event):
	if event is InputEventScreenDrag:
		movimenta=true
		if mover_control_pai:
			get_parent().get_node("Control").set_global_position(Vector2(event.position.x,event.position.y))
			mover_control_pai=false
		else:
			get_parent().get_node("Control/Seta_cabeca").set_global_position(Vector2(event.position.x,event.position.y))
		
		
		if get_parent().get_node("Control/Seta_cabeca").get_position().x>=seta_right.x-50:
			get_parent().get_node("Control/Seta_cabeca").position.x=seta_right.x-50
					
		if get_parent().get_node("Control/Seta_cabeca").get_position().x<=seta_left.x+50:
			get_parent().get_node("Control/Seta_cabeca").position.x=seta_left.x+50
		if get_parent().get_node("Control/Seta_cabeca").get_position().y<=seta_up.y+50:
			get_parent().get_node("Control/Seta_cabeca").position.y=seta_up.y+50
		if get_parent().get_node("Control/Seta_cabeca").get_position().y>=seta_down.y-50:
			get_parent().get_node("Control/Seta_cabeca").position.y=seta_down.y-50
			
	
	if event is InputEventScreenTouch:
		get_parent().get_node("Control").set_position(posicao_control_original)
		get_parent().get_node("Control/Seta_cabeca").set_position(posicao_cabeca_original)
		mover_control_pai=true
		movimenta=false
		
		

		
			
func movimentacao2(delta):
	if movimenta:
		if right and up:
			translate(velocidade*delta*Vector2(1,-1))
		elif right and down:
			translate(velocidade*delta*Vector2(1,1))
		elif left and up:
			translate(velocidade*delta*Vector2(-1,-1))
		elif left and down:
			translate(velocidade*delta*Vector2(-1,1))
		elif right:
			translate(velocidade*delta*Vector2(1,0))
		elif left:
			translate(velocidade*delta*Vector2(-1,0))
		elif up:
			translate(velocidade*delta*Vector2(0,-1))
		elif down:
			translate(velocidade*delta*Vector2(0,1))
			
#----- fim da implementação para controlar o carro com o direcional virtual------
		
func _on_Corpo_area_entered(area):
	if area.has_method("carro_bazuca"):
		PainelSingleton.reduzir_energia(area.carro_bazuca())
		destruir_carro()
	if area.has_method("bazuca"):
		explosao_total()
		
		PainelSingleton.reduzir_energia(area.bazuca())
		area.queue_free()
	if area.has_method("cepo_espinho"):
		$Sons/Cepo.play()
		PainelSingleton.reduzir_energia(area.cepo_espinho())
		get_parent().get_node("Zoom").play("zoom_cepo")
		get_parent().get_node("Esbranquicar").play("esbranqu")
		destruir_carro()
	if area.has_method("tiro_chefao"):
		get_parent().get_node("Esbranquicar").play("esbranqu")
		PainelSingleton.reduzir_energia(area.danificar_heroi())
		
		destruir_carro()
		$Sons/Pneus.play()
		get_parent().get_node("Efeito_chefao").play("impacto")
		$Timer_pipocos.start()
		
	if area.has_method("tiro_helicopter"):
		get_parent().get_node("Esbranquicar").play("esbranqu")
		PainelSingleton.reduzir_energia(area.tiro_helicopter())
		
		destruir_carro()
		$Sons/Pneus.play()
		get_parent().get_node("Efeito_chefao").play("impacto")
		$Timer_pipocos.start()
			
		
	if area.has_method("moto"):	
		PainelSingleton.reduzir_energia(area.danificar_carro())
		destruir_carro()
		if pegou_power==false:
			get_parent().get_node("EfeitoCorAnime").play("esbranquicar")
		batida_leve()
	if area.has_method("ataque_muchacho"):
		$Sons/Batida_metal.play()
		PainelSingleton.reduzir_energia(area.ataque_muchacho())
		if pegou_power==false:
			get_parent().get_node("EfeitoCorAnime").play("esbranquicar")
		batida_leve()
	if area.has_method("limite_esquerdo"):
		
		limite_esquerdo=area.get_global_position().x
	if area.has_method("limite_direito"):
		limite_direito=area.get_global_position().x
	
	if area.has_method("carro_chefe") or area.has_method("carro_caminhao"):
		PainelSingleton.reduzir_energia(area.danificar_heroi())
		destruir_carro()
		var faisca = faisca_batida.instance()
		var posicao = to_local(get_global_position())
		faisca.set_position(posicao)
		add_child(faisca)
		if pegou_power==false:
			get_parent().get_node("Esbranquicar").play("esbranqu")
		$Sons/Batida_metal.play()
		if area.get_global_position().x<get_global_position().x:
			set_global_position(get_global_position()+Vector2(20,0))
		elif area.get_global_position().x>get_global_position().x:
			set_global_position(get_global_position()+Vector2(-20,0))
		if area.get_global_position().y<get_global_position().y:
			set_global_position(get_global_position()+Vector2(0,20))
		elif area.get_global_position().x>get_global_position().x:
			set_global_position(get_global_position()+Vector2(0,-20))
		get_parent().get_node("Efeito_chefao").play("batida")
	if area.has_method("batida_corpo_inimigo"):
		PainelSingleton.reduzir_energia(area.danificar_heroi())	
		destruir_carro()
		if pegou_power==false:
			get_parent().get_node("EfeitoCorAnime").play("esbranquicar")
		movimenta=false
		batida_leve()
		
	if area.has_method("canteiro"):
		
		get_parent().get_parent().reduzir_velocidade_pista(0.5)
		$Areias.show()
		get_parent().get_node("AnimationPlayer").play("desiquilibrio")
	
	if area.has_method("buraco"):
		get_parent().get_node("Zoom").play("Zoom_menos")
		get_parent().get_node("AnimationPlayer").play("desiquilibrio")
	if area.has_method("barreira"):
		$Areias.hide()
		$Sons/Explosao.play()
		PainelSingleton.reduzir_energia(PainelSingleton.retornar_energia_heroi())
		set_collision_layer_bit(12,false)
		var x_are=area.get_global_position().x
		var x_her=get_global_position().x
		
		operando=false
		
		if(x_are-x_her)>=82.0:
			$AnimeCaminhonete.set_frame(6)
		elif(x_her-x_are)>=97.0:
			$AnimeCaminhonete.set_frame(5)
		else:
			$AnimeCaminhonete.set_frame(4)
			
		parar_pista()
		get_parent().get_node("EfeitosAcessorios").play("mov_p_tras_suave")
		$Fumaca.show()
		$Fumaca/Efeito.play("explosion")
		if PainelSingleton.retornar_energia_heroi()<=0:
			PainelSingleton.exibir_continue()
			PainelSingleton.set_vivo(false)
			operando=false
	
	if area.has_method("rampa"):
		pass
		#$Sons/Salto.play()
		#$AnimeCaminhonete.set_frame(7)
	if area.has_method("buraco_fundo"):
		$Areias.hide()
		hide()
		$Armas.hide()
		get_parent().get_node("Control").hide()
		
		parar_pista()
		operando=false
		get_parent().get_node("Zoom").play("Caindo")
		PainelSingleton.reduzir_energia(PainelSingleton.retornar_energia_heroi())
		if PainelSingleton.retornar_energia_heroi()<=0:
			PainelSingleton.exibir_continue()
			PainelSingleton.set_vivo(false)
	if area.has_method("buraco_mar"):
		
		parar_pista()
		get_parent().hide()
		area.iniciar_animacao()
		
		PainelSingleton.reduzir_energia(PainelSingleton.retornar_energia_heroi())
	
	if area.has_method("sinal_raio") and PainelSingleton.estah_vivo():
		esconder_labels()
		som_power()
		$Sons/Shock.play()
		get_parent().get_node("EfeitosTextos").play("Shock")
		area.destruir()
		$Armas/Choque.show()
		$Armas/Choque/Choque.set_ativar(true)
		$Armas/Choque/Timer.start()
	if area.has_method("sinal_rapidez") and PainelSingleton.estah_vivo():
		esconder_labels()
		som_power()
		get_parent().get_node("EfeitoCorAnime").play("Power")
		get_parent().get_node("EfeitosTextos").play("Speed")
		velocidade=velocidade+200
		$Sons/Speed.play()
		area.destruir()
		$Regularizar_velocidade.start()
	if area.has_method("sinal_escudo") and PainelSingleton.estah_vivo():
		esconder_labels()
		som_power()
		$Sons/Shield.play()
		get_parent().get_node("EfeitoCorAnime").play("Power")
		get_parent().get_node("EfeitosTextos").play("shield")
		$Armas/Frente_ataque.show()
		$Armas/Frente_ataque.set_ativar(true)
		
		area.destruir()
	if area.has_method("sinal_missel") and PainelSingleton.estah_vivo():
		animar_coleta_missel()
		esconder_labels()
		get_parent().get_node("Control/TouchScreenButton").show()
		som_power()
		$Sons/Missiles.play()
		get_parent().get_node("EfeitoCorAnime").play("Power")
		get_parent().get_node("EfeitosTextos").play("Misseles")
		exibir_lancador_com_missel()
		area.destruir()
		contador_missel=2
	if area.has_method("sinal_life") and PainelSingleton.estah_vivo():
		limpar_fumaca()
		esconder_labels()
		som_power()
		$Sons/Energy.play()
		get_parent().get_node("EfeitoCorAnime").play("Power")
		get_parent().get_node("EfeitosTextos").play("Energy")
		PainelSingleton.ganhar_energia(area.get_power())
		area.destruir()
	if area.has_method("bala"):
		destruir_carro()
		$Sons/Batida_bala.play()
		if pegou_power==false:
			get_parent().get_node("EfeitoCorAnime").play("esbranquicar")
		batida_leve()
	if area.has_method("dinamite"):
		$Sons/Batida_metal.play()
		$Sons/Explosao.play()
		destruir_carro()
		if pegou_power==false:
			get_parent().get_node("EfeitoCorAnime").play("esbranquicar")
		
		
		
func batida_leve():
	get_parent().get_node("AnimationPlayer").play("batida_leve")
		


func _on_Corpo_area_exited(area):
	if area.has_method("canteiro"):
		get_parent().get_parent().normalizar_velocidade_pista()
		$Areias.hide()
		get_parent().get_node("AnimationPlayer").stop()
		
	if area.has_method("buraco"):
		get_parent().get_node("Zoom").play("Normal")
		get_parent().get_node("AnimationPlayer").stop()
		
	if area.has_method("rampa"):
		pass
		#get_parent().get_node("EfeitosAcessorios").play("Salto")
		
func frame_padrao():
	
	$AnimeCaminhonete.set_frame(0)
func frame_carro_pra_baixo():
	$AnimeCaminhonete.set_frame(8)
func ir_para_atras():
	set_global_position(Vector2(get_global_position().x,get_global_position().y+5))

func parar_pista():
	PainelSingleton.parar_pista()

func explodir_carro():
	$Fumaca.show()
	$Fumaca/Efeito.play("explosion")
func aumentar_chama():
	$Fumaca/Sprite2.set_scale(Vector2(2.5,2.5))

func mergulhar():
	$Efeitos/Mergulho/CPUParticles2D.set_emitting(true)
	$AnimeCaminhonete.hide()
	$Sombra.hide()

	
func desativar_powers():
	$Armas/Choque.hide()
	$Armas/Choque/Choque.set_ativar(false)
	velocidade=200
	
func ocultar_lancador_com_missel():
	$Armas/Lancador_missel/direito/Missel.hide()
	$Armas/Lancador_missel/esquerdo/Missel.hide()
	$Armas/Lancador_missel.hide()
func exibir_lancador_com_missel():
	$Armas/Lancador_missel.show()
	$Armas/Lancador_missel/direito/Missel.show()
	$Armas/Lancador_missel/esquerdo/Missel.show()
	
func _on_Regularizar_velocidade_timeout():
	velocidade=200
func heroi():
	pass
func explosao_total():
	var explodir = explosao_g.instance()
	explodir.set_global_position(get_global_position())
	get_parent().get_parent().add_child(explodir)
	explodir.get_node("AnimatedSprite").playing=true
	$AnimeCaminhonete.hide()
	$Sombra.hide()
	$Armas.hide()
	parar_pista()
	$Sons/Explosao.play()
	operando=false
	PainelSingleton.set_vivo(false)
	PainelSingleton.exibir_continue()
	
func destruir_carro():
	if PainelSingleton.retornar_energia_heroi()<=0:
		parar_pista()
		$Sons/Explosao.play()
		explodir_carro()
		operando=false
		PainelSingleton.set_vivo(false)
		PainelSingleton.exibir_continue()
		
func operar_carro():
	operando=true
func som_power():
	$Sons/Power.play()

func _on_EfeitoCorAnime_animation_started(anim_name):
	
	if anim_name=="Power":
		pegou_power=true


func _on_EfeitoCorAnime_animation_finished(anim_name):
	if anim_name=="Power":
		pegou_power=false



func _on_Zoom_animation_finished(anim_name):
	if anim_name=="Caindo":
		$Sons/Explosao.play()

func esconder_labels():
	for a in $Labels.get_children():
		a.hide()
func deixar_azul_seta(caminho:String):
	get_parent().get_node("Control/"+caminho+"/Azul").show()
	get_parent().get_node("Control/"+caminho+"/Transp").hide()
func deixar_transparente_seta(caminho:String):
	get_parent().get_node("Control/"+caminho+"/Azul").hide()
	get_parent().get_node("Control/"+caminho+"/Transp").show()
	

func _on_Seta_up_area_entered(_area):
	
	deixar_azul_seta("Seta_up")
	up=true
	if right:
		get_parent().get_node("Direcao").play_backwards("direita")
	elif left:
		get_parent().get_node("Direcao").play_backwards("esquerda")


func _on_Seta_up_area_exited(_area):
	deixar_transparente_seta("Seta_up")
	up=false
	


func _on_Seta_right_area_entered(_area):
	deixar_azul_seta("Seta_right")
	right=true
	if operando:
		if !up and !down:
			get_parent().get_node("Direcao").play("direita")


func _on_Seta_right_area_exited(_area):
	deixar_transparente_seta("Seta_right")
	right=false
	if operando:
		get_parent().get_node("Direcao").play_backwards("direita")


func _on_Seta_down_area_entered(_area):
	deixar_azul_seta("Seta_down")
	down=true
	if right:
		get_parent().get_node("Direcao").play_backwards("direita")
	elif left:
		get_parent().get_node("Direcao").play_backwards("esquerda")


func _on_Seta_down_area_exited(_area):
	deixar_transparente_seta("Seta_down")
	down = false
	


func _on_Seta_left_area_entered(_area):
	deixar_azul_seta("Seta_left")
	left=true
	if operando:
		if !up and !down:
			get_parent().get_node("Direcao").play("esquerda")


func _on_Seta_left_area_exited(_area):
	deixar_transparente_seta("Seta_left")
	left=false
	if operando:
		get_parent().get_node("Direcao").play_backwards("esquerda")


func _on_TouchScreenButton_pressed():
	
	toque+=1
	if toque==2:
		if contador_missel==2 and PainelSingleton.estah_vivo():
			var miss = missel2.instance()
			miss.set_global_position($Armas/Lancador_missel/direito/Missel.get_global_position())
			$Armas/Lancador_missel/direito/Missel.hide()
			get_parent().get_parent().add_child(miss)
			contador_missel=contador_missel-1
		
		elif contador_missel==1 and PainelSingleton.estah_vivo():
			var miss = missel2.instance()
			miss.set_global_position($Armas/Lancador_missel/esquerdo/Missel.get_global_position())
			$Armas/Lancador_missel/esquerdo/Missel.hide()
			get_parent().get_parent().add_child(miss)
			contador_missel=contador_missel-1
		if contador_missel==0:
			get_parent().get_node("Control/TouchScreenButton").hide()
			$Armas/Lancador_missel.hide()


func _on_Timer_pipocos_timeout():
	contador_explosoes+=1
	
	var explo = pipoco.instance()
	explo.set_scale(Vector2(0.2,0.2))
	$AnimeCaminhonete.add_child(explo)
	var posic_x = rand_range(-100,100)
	var posic_y = rand_range(-400,-100)
	explo.set_position(Vector2(posic_x,posic_y))
			
	explo.get_node("AnimatedSprite").playing=true
	$Sons/Explosao_rapida.play()
	if contador_explosoes>5:
		var f = fuma.instance()
		f.set_scale(Vector2(2,1.5))
		$AnimeCaminhonete.add_child(f)
		f.set_position(Vector2(posic_x,posic_y))
		$Timer_pipocos.stop()
		contador_explosoes=0
		
func limpar_fumaca():
	for i in $AnimeCaminhonete.get_children():
		if i != null:
			i.queue_free()

func _on_Efeito_chefao_animation_started(anim_name):
	if anim_name=="batida":
		operando=false
		batid_chefao=true
		


func _on_Efeito_chefao_animation_finished(_anim_name):
	
		operando=true
		batid_chefao=false

func parar_controle_carro():
	if !PainelSingleton.estah_vivo():
		operando=false
	if PainelSingleton.get_movimentar_pista():
		operando=false
	if PainelSingleton.get_parar_pista2():
		operando=false 
func movimentar_carro_final(delta):
	
	if PainelSingleton.get_parar_pista2():
		if $Armas/Frente_ataque.is_visible_in_tree() and destruir_pontas:
			$Armas/Frente_ataque.destruir_final()
			destruir_pontas=false
		if(get_global_position().y>1080):
			velocidade=0
		translate(velocidade*delta*Vector2(0,1))
		


func _on_Time_zerar_toques_timeout():
	toque=0


func _on_TouchScreenButton_released():
	get_parent().get_node("Time_zerar_toques").start()

func animar_coleta_missel():
	if anime_missel<=3:
		$Anime_botao.show()
		$Anime_botao.playing=true
		anime_missel+=1
		

func _on_Anime_botao_animation_finished():
	$Anime_botao.hide()
	$Anime_botao.playing=false
	$Anime_botao.set_frame(0)
