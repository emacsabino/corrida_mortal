extends Node2D
var fase
func _ready():
	fase = save_game()
	if fase=="Pista0" or fase.empty():
		$Continue.hide()
	else:
		$Continue.show()


func _on_TouchScreenButton_pressed():
	$Start.play()
	$Time_Iniciar.start()


func _on_Timer_timeout():
	if get_tree().change_scene("res://Scenes/pistas/"+fase+".tscn")!=OK:
		print ("An unexpected error occured when trying to switch to the Readme scene")


func _on_Continue_pressed():
	$Start.play()
	$Timer.start()


func _on_Time_Iniciar_timeout():
		if get_tree().change_scene("res://Scenes/pistas/Pista0.tscn"):
			print ("An unexpected error occured when trying to switch to the Readme scene")

func save_game():
	var file = File.new()
	file.open("user://save_game.dat",file.READ)
	var content = file.get_as_text()
	file.close()
	return content


func _on_Info_pressed():
	$Info/Sprite.show()
	$Info/Button.show()


func _on_Button_button_down():
	$Info/Sprite.hide()
	$Info/Button.hide()
