extends Node2D
export var roupa:int=1
var player
export var ativar:bool=false
var atirar = false
var perseguir = true
var bomba = preload("res://Scenes/pistas/Inimigos/Bomba_bazuca.tscn")
func _ready():
	randomize()
	if ativar:
		var tim = rand_range(3.0,4.1)
		$Timer_Atirar.wait_time=tim
		$Timer_Atirar.start()
	var image = load("res://Assets/inimigos/homem_bazuca/"+str(roupa)+".png")
	$Homem_bazuca.set_deferred("texture",image)
	
	player= get_node("/root/Pista0/Carro/Corpo")
func _process(_delta):
	if atirar:
		atirar_bomba()
		atirar=false
	if perseguir:
		$Homem_bazuca.look_at(player.get_global_position())
func atirar_bomba():
	var bomb = bomba.instance()
	bomb.set_global_position($Homem_bazuca/Bomba.get_global_position())
	
	get_parent().get_parent().get_parent().add_child(bomb)
	$Som_tiro.play()
	perseguir=false
	$Homem_bazuca/Bomba.hide()
	

func _on_Timer_Atirar_timeout():
	atirar=true
