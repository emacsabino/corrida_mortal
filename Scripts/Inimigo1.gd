extends "Inimigo_pai.gd"

var explosion=true
const pontos=20
var batida_pos_giro=0
var velocidade
var direcao
var apareceu_tela=false
var alvo_do_missel_right=false
var alvo_do_missel_left=false
var mx=[-0.3,0,0.4]
var qtd_impac_sofrido=0
var posicao_x = [220,488]
var posicao_escolha
var vivo = true
var ativar_movimentacao=true
var ativar_lado_direito=false
var ativar_lado_esquerdo=false
var morte1=false
var morte2=false
var morte3=false
var morte4=false
var posicao_area
var posicao_self
var entrou_na_areia=false
var difer_x=0
var difer_y=0
var voltar=true
var voando_girando = true
var voando = true
var posicao_inicial_esquerda=false
var explosao2 = preload("res://Scenes/pistas/Inimigos/Inimigo4.tscn")
# Called when the node enters the scene tree for the first time.
func _ready():
	
	set_process(true)
	randomize()
	var t = rand_range(0,2)
	posicao_escolha =posicao_x[int(t)]
	if posicao_escolha<=220:
		set_posicao_inicial_esquerda(true)
	set_global_position(Vector2(posicao_escolha,-134.083))
	
	
	velocidade=rand_range(150,250)
	
	
	#var m = rand_range(0,3)
	direcao = Vector2(mx[int(1)],direcao_pai_y)
	var x = rand_range(0,3)
	
	$Corpo/AnimeCarro.set_frame(int(x))
	
func _process(delta):
	if PainelSingleton.get_movimentar_pista():
		
		direcao.y=-5
	#print($Giro.current_animation)
	ir_pra_frente_lado_direito()
	ir_pra_frente_lado_esquerdo()

	if alvo_do_missel_right:
		PainelSingleton.set_posicao_inimigo_right(get_global_position())
	if alvo_do_missel_left:
		
		PainelSingleton.set_posicao_inimigo_left(get_global_position())
	if vivo:
		if ativar_movimentacao:
			translate(velocidade*direcao*delta)
	else:
	
		morrer(delta,posicao_area,posicao_self)
	
	destruir()
		

				




func destruir():
	
	if get_global_position().y>=1500:
		PainelSingleton.reduzir_carros()
		PainelSingleton.pontuar(pontos)
		queue_free()		
	if get_global_position().x>1000:
		PainelSingleton.reduzir_carros()
		PainelSingleton.pontuar(pontos)
		queue_free()
	if get_global_position().x<-200:
		PainelSingleton.reduzir_carros()
		PainelSingleton.pontuar(pontos)
		queue_free()
		
func explodir():
	if explosion:
		direcao.y=5
		velocidade=400
		$Corpo/AnimeCarro.set_modulate(Color.black)
		$Explosao.show()
		$Explosao/AnimeExplosion.play("explosao")
		$Fumacas.show()
		explosion=false
		vivo=false


func _on_Corpo_area_entered(area):
	if area.has_method("buraco"):
		$Zoom.play("zoom_menos")
		$Giro.play("desequilibrio")
	
	if area.has_method("batida_leve"):
		exibir_fumacas()
		if batida_pos_giro>0:
			explodir()
			morte1=false
			morte2=true
			morte3=false
			morte4=false
			
		
		var area_p=area.get_global_position()
		var inim_p=get_global_position()
	
		contar_batidas_sofridas(area_p,inim_p)
		$Giro.play("batida_leve")
		$EfeitoCorAnime.play("esbranquicar")
		
		if (area_p.x<inim_p.x)and(area_p.y-inim_p.y<200):
			arrastando_direita(100)
			
			
			direcao.x=-direcao.x
			if abs(area_p.y-inim_p.y)<50:
				emitir_brilho_left()
				$Acessorios/lat_esq_abai.show()
				$Acessorios/lat_esq_acim.show()
			
		elif (area_p.x>inim_p.x)and(area_p.y-inim_p.y<170):
			arrastando_esquerda(100)
			
			
			direcao.x=-direcao.x
			if abs(area_p.y-inim_p.y)<50:
				emitir_brilho_right()
				$Acessorios/lat_dir_abaix.show()
				$Acessorios/lat_dir_acim.show()
			
			
		elif (area_p.x>inim_p.x)and(area_p.y-inim_p.y>170 and area_p.y-inim_p.y<235):
			$Giro.play("giro_completo_esquerda")
			batida_pos_giro=batida_pos_giro+1
			desativar_mask_parede()
			arrastando_esquerda(60)
			direcao.x=-1
			direcao.y=3
		elif (area_p.x<inim_p.x)and(area_p.y-inim_p.y>170 and area_p.y-inim_p.y<235):
			$Giro.play("giro_completo_direita")
			batida_pos_giro=batida_pos_giro+1
			desativar_mask_parede()
			arrastando_direita(60)
			direcao.x=1
			direcao.y=3
		elif (area_p.x>inim_p.x)and(area_p.y-inim_p.y>235):
			arrastando_cima(60)
			direcao.y=-1
			$Pos_batida_traseira.start()
		elif (area_p.x<inim_p.x)and(area_p.y-inim_p.y>235):
			arrastando_cima(60)
			direcao.y=-1
			$Pos_batida_traseira.start()
			
	if area.has_method("parede"):
		
		
		direcao.x=-direcao.x
	
	if area.has_method("batida_corpo_inimigo"):
		exibir_fumacas()
		
		var pos_car=get_global_position()
		var pos_area=area.get_global_position()
		difer_x=pos_area.x-pos_car.x
		difer_y=pos_area.y-pos_car.y
		
		contar_batidas_sofridas(pos_area,pos_car)
		if (pos_area.y>pos_car.y) and (abs(difer_x)<109):
			area.parar()
			arrastando_cima(40)
			if esta_a_esquerda():
				ativar_lado_direito=true
				direcao.y=0
				direcao.x=1
				
			else:
				ativar_lado_esquerdo=true
				direcao.y=0
				direcao.x=-1
		elif (pos_area.y<pos_car.y) and (abs(difer_x)<109):
			arrastando_direita(30)
			parar()
			$Ativar_movimentacao.start()
		
		
		elif (pos_area.y<pos_car.y) and (difer_x>109):
			arrastando_esquerda(30)
			parar()
			$Ativar_movimentacao.start()
	if area.has_method("canteiro"):
		
		$Giro.play("desequilibrio")
		$Areias.show()
		velocidade=400
		entrou_na_areia=true
	if area.has_method("missel"):
		var explo = explosao2.instance()
		$Corpo.hide()
		$Explosao_missel/Explosao2.show()
		explo.set_global_position(get_global_position())
		get_parent().get_parent().add_child(explo)
		explo.get_node("AnimatedSprite").play()
		PainelSingleton.pontuar(pontos)
		queue_free()
	if area.has_method("choque"):
		vivo=false
		$Choque.show()
		morte4=true
		
		
		
func _on_Corpo_area_exited(area):
	if area.has_method("canteiro"):
		$Giro.stop()
		$Areias.hide()
		velocidade=250
		entrou_na_areia=false
	if area.has_method("buraco"):
		
		$Zoom.play("normal")
		$Giro.stop()
			
func arrastando_direita(valor):
	set_global_position(Vector2(get_global_position().x+valor,get_global_position().y))
	
func arrastando_esquerda(valor):
	set_global_position(Vector2(get_global_position().x-valor,get_global_position().y))

func arrastando_cima(valor):
	set_global_position(Vector2(get_global_position().x,get_global_position().y-valor))
		
func arrastando_baixo(valor):
	set_global_position(Vector2(get_global_position().x,get_global_position().y+valor))
		
func _on_Pos_batida_traseira_timeout():
	direcao.y=1
	var x = rand_range(0,3)
	direcao.x=mx[int(x)]
func desativar_mask_parede():
	$Corpo.set_collision_mask_bit(2,false)

func exibir_fumacas():
	
	if qtd_impac_sofrido>=1:
		$Fumacas.show()
func contar_batidas_sofridas(area_pos,self_pos):
	posicao_area=area_pos
	posicao_self=self_pos
	qtd_impac_sofrido=qtd_impac_sofrido+1
	if qtd_impac_sofrido>3:
			morte1=true
			vivo=false
func esta_a_esquerda():
	var par_esq=PainelSingleton.get_position_parede_esq()
	var par_dir= PainelSingleton.get_position_parede_dir()
	var get_pos=get_global_position()
	if (get_pos.x-par_esq.x)<=(par_dir.x-get_pos.x):
		return true
	else:
		return false	
	
	
func ir_pra_frente_lado_direito():
	if ativar_lado_direito and get_global_position().x>445:
		direcao=Vector2(0,1)
		ativar_lado_direito=false
func ir_pra_frente_lado_esquerdo():
	if ativar_lado_esquerdo and get_global_position().x<239:
		direcao=Vector2(0,1)
		ativar_lado_esquerdo=false
func parar():
	ativar_movimentacao=false
	$Ativar_movimentacao.start()

func _on_Ativar_movimentacao_timeout():
	ativar_movimentacao=true

func morrer(delta,pos_area,pos_self):
	
	if morte1:
		desativar_mask_parede()
		if pos_area.y-pos_self.y>235:
			translate(Vector2(0,2)*400*delta)
			$Corpo.desativar_mask()
			if voando_girando:
				efeito_zoom("voando_girando")
		elif pos_area.x>pos_self.x:
			translate(Vector2(-1.2,0.5)*400*delta)
			if voando_girando:
				efeito_zoom("voando_girando")
		elif pos_area.x<pos_self.x:
			translate(Vector2(1.2,0.5)*400*delta)
		
	elif morte2:
		desativar_mask_parede()
		if pos_area.x>pos_self.x:
			translate(Vector2(-1.2,0.5)*400*delta)
			if voando:
				efeito_zoom("voando")
		elif pos_area.x<pos_self.x:
			translate(Vector2(1.2,0.5)*400*delta)
		if pos_area.y-pos_self.y>235:
			$Corpo.desativar_mask()
			if voando:
				efeito_zoom("voando")
	elif morte3:
		morte_por_sobreposicao(delta)
	elif morte4:
		morte_por_choque(delta)
			
			
			
func efeito_zoom(anime):
	$Corpo.desativar_mask()
	$Voando.play(anime)

		
func morte_por_sobreposicao(delta):
	$Sobrepor.set_collision_mask_bit(10,false)
	$Sobrepor.set_collision_layer_bit(10,false)
	$Corpo.desativar_mask()
	if voando_girando:
		$Voando.play("voando_girando")
		$Corpo/AnimeCarro.set_modulate(Color.black)
		
	$Explosao.show()
	$Explosao/AnimeExplosion.play("explosao")
	translate(Vector2(0,2)*400*delta)

func alterar_velocidade(veloci):
	velocidade=veloci
func progredir_velocidade():
	$Normalizar_velocidade.start()
func get_entrou_na_areia():
	return entrou_na_areia




func _on_Giro_animation_started(anim_name):
	#desativei as máscaras para que o efeito de giro não fosse cortado
	if (anim_name=="giro_completo_direita") or (anim_name=="giro_completo_esquerda"):
		$Corpo.set_collision_mask_bit(6,false)
		$Corpo.set_collision_mask_bit(7,false)
		set_voltar(false)


func _on_Sobrepor_area_entered(area):
	$Sobrepor.set_collision_mask_bit(9,false)
	$Sobrepor.set_collision_layer_bit(9,false)
	
	if area.get_global_position().y>get_global_position().y:
		vivo = false
		morte1=false
		morte2=false
		morte3=true
		morte4=false
	
	

func set_voltar(valor:bool):
	voltar=valor
	
func get_voltar():
	return voltar

func _on_Voando_animation_started(anim_name):
	if anim_name=="voando_girando":
		voando_girando=false
		set_voltar(false)
	if anim_name=="voando":
		voando=false
		set_voltar(false)
func emitir_brilho_right():
	$Brilhos/later_direita/brilho_batida.show()
	$Brilhos/later_direita/brilho_batida2.show()
	$Brilhos/later_direita/brilho_batida3.show()
	$Brilhos/later_direita/brilho_batida/brilho.set_emitting(true)
	$Brilhos/later_direita/brilho_batida2/brilho.set_emitting(true)
	$Brilhos/later_direita/brilho_batida3/brilho.set_emitting(true)
func emitir_brilho_left():
	$Brilhos/later_esquerda/brilho_batida.show()
	$Brilhos/later_esquerda/brilho_batida2.show()
	$Brilhos/later_esquerda/brilho_batida3.show()
	$Brilhos/later_esquerda/brilho_batida/brilho.set_emitting(true)
	$Brilhos/later_esquerda/brilho_batida2/brilho.set_emitting(true)
	$Brilhos/later_esquerda/brilho_batida3/brilho.set_emitting(true)
	

func _on_fundo_carro_area_entered(area):
	$fundo_carro/brilho_batida.show()
	$fundo_carro/brilho_batida2.show()
	$fundo_carro/brilho_batida3.show()
	$fundo_carro/brilho_batida/brilho.set_emitting(true)
	$fundo_carro/brilho_batida2/brilho.set_emitting(true)
	$fundo_carro/brilho_batida3/brilho.set_emitting(true)
	$Acessorios/fundo.show()
	$Acessorios/lanterna_direita.show()
	$Acessorios/lanterna_esquerda.show()
	#$fundo_carro.queue_free()



func _on_Normalizar_velocidade_timeout():
	velocidade=velocidade+100
	if velocidade>250:
		$Normalizar_velocidade.stop()

func set_alvo_para_missel(right:bool):
	if right:
		alvo_do_missel_right=right
		PainelSingleton.set_alvo_missel_right(false)
	else:
		
		alvo_do_missel_left=!right
		
		PainelSingleton.set_alvo_missel_left(false)

func _on_VisibilityNotifier2D_screen_entered():
	apareceu_tela=true
func get_apareceu_tela():
	return apareceu_tela
func get_vivo():
	return vivo


func _on_VisibilityNotifier2D_screen_exited():
	apareceu_tela=false

func get_alvo_do_missel_right():
	return alvo_do_missel_right
	
func morte_por_choque(delta):
	if posicao_inicial_esquerda:
		
		translate(400*Vector2(-1,0)*delta)
	else:
		translate(400*Vector2(1,0)*delta)
func set_posicao_inicial_esquerda(valor:bool=false):
	posicao_inicial_esquerda=valor


