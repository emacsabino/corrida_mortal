extends Area2D
var direcao = Vector2(0,1)
var velocidade = 500
var entrada = true
var parti = preload("res://Scenes/particulas/batida_chefe.tscn")
var zigzag = false
var batida = false
var perseguir = true

var life = 3000
var tiro_chefe = preload("res://Scenes/tiro_chefao.tscn")
var player
var atirar_chefao=true
func _ready():
	
	player = get_node("/root/Pista0/Carro/Corpo")
	
func _process(delta):
	if PainelSingleton.get_parar_pista_total():
			batida = false
			zigzag = false
			entrada = false
	if PainelSingleton.get_movimentar_pista() and life>0:
		direcao.y=-1
		velocidade=2000
		direcao.x=0
		zigzag=false
		translate(direcao*delta*velocidade)
	if batida:
		translate(velocidade*delta*Vector2(0,-1))
	else:
		entrad(delta)
		zig_zag(delta)
	if perseguir:
		$Atirador/Sprite.look_at(player.get_global_position())
	
	

func entrad(del):
	if entrada:
		translate(direcao*velocidade*del)
		if get_global_position().y>=390:
			direcao.x=0.4
func zig_zag(del):
	if get_global_position().y>=400:
		entrada=false
		zigzag=true
		
	if zigzag:
		if get_global_position().y>=400:
			direcao.y=-0.4
		elif get_global_position().y<=100:
			direcao.y=0.4
		
		if get_global_position().x>=550:
			direcao.x=-0.4
		elif get_global_position().x<=200:
			direcao.x=0.4
		translate(direcao*velocidade*del)
		
	
	



func _on_Tempo_tiro_timeout():
	if atirar_chefao:
		$Atirador/Sprite/nasc_tiro/Pipoco.playing=true
		var bal = tiro_chefe.instance()
		bal.set_global_position($Atirador/Sprite/nasc_tiro.get_global_position())
		bal.get_node("corpo").set_rotacao($Atirador/Sprite.rotation)
		
		get_parent().get_parent().add_child(bal)


func _on_Pipoco_animation_finished():
	$Atirador/Sprite/nasc_tiro/Pipoco.set_frame(0)
	$Atirador/Sprite/nasc_tiro/Pipoco.playing=false

func batida_corpo_inimigo():
	pass
func danificar_heroi():
	return 5
func _on_Corpo_area_entered(area):
	if area.has_method("heroi"):
		$Batida.play()
		reduzir_energia(5)
		if area.get_global_position().x<get_global_position().x:
			set_global_position(get_global_position()+Vector2(20,0))
		elif area.get_global_position().x>get_global_position().x:
			set_global_position(get_global_position()+Vector2(-20,0))
		if area.get_global_position().y<get_global_position().y:
			set_global_position(get_global_position()+Vector2(0,20))
		elif area.get_global_position().x>get_global_position().x:
			set_global_position(get_global_position()+Vector2(0,-20))

	if area.has_method("frente_pontas"):
		
		var brilh = parti.instance()
		var pos = to_local(area.get_global_position())
		add_child(brilh)
		brilh.set_position(pos)
		brilh.get_node("CPUParticles2D").emitting=true
		
		
		$Estrondo.play()
		reduzir_energia(area.criar_dano())
		
		set_global_position(Vector2(get_global_position().x,get_global_position().y-50))
		get_parent().get_node("Esbranquicar").play("esbranquic")
		get_parent().get_node("batida_efeito").play("batida")
		exibir_danos()
func reduzir_energia(valor):
	life = life - valor
func carro_chefe():
	pass
	
		

func _on_batida_efeito_animation_started(anim_name):
	if !PainelSingleton.get_movimentar_pista():
		batida=true
		$Timer.start()

func _on_Timer_timeout():
	batida=false
func exibir_danos():
	
	
	if life<2500:
		var expl = parti.instance()		
		$danos/Dano1.add_child(expl)		
		expl.get_node("CPUParticles2D").emitting=true
		$danos/Dano1.show()
	if life<2000:
		var expl = parti.instance()
		$danos/Dano2.add_child(expl)	
		expl.get_node("CPUParticles2D").emitting=true
		$danos/Dano2.show()
	if life<1800:
		var expl = parti.instance()
		$danos/Dano3.add_child(expl)
		expl.get_node("CPUParticles2D").emitting=true
		$danos/Dano3.show()
	if life<1500:
		var expl = parti.instance()
		$danos/Dano4.add_child(expl)	
		expl.get_node("CPUParticles2D").emitting=true
		$danos/Dano4.show()
	if life<1200:
		var expl = parti.instance()
		$danos/Dano5.add_child(expl)	
		expl.get_node("CPUParticles2D").emitting=true
		$danos/Dano5.show()
	if life<1000:
		var expl = parti.instance()
		$danos/Dano6.add_child(expl)	
		expl.get_node("CPUParticles2D").emitting=true
		$danos/Dano6.show()
	if life<800:
		var expl = parti.instance()
		$danos/Dano7.add_child(expl)	
		expl.get_node("CPUParticles2D").emitting=true
		$danos/Dano7.show()
	if life<500:
		var expl = parti.instance()
		$danos/Dano8.add_child(expl)	
		expl.get_node("CPUParticles2D").emitting=true
		$danos/Dano8.show()
	if life<300:
		var expl = parti.instance()
		$danos/Dano9.add_child(expl)	
		expl.get_node("CPUParticles2D").emitting=true
		$danos/Dano9.show()
	if life<=0:
		PainelSingleton.parar_pista2()
		atirar_chefao=false
		$Atirador.hide()
		
		var expl = parti.instance()
		$danos/Morto.add_child(expl)
		expl.get_node("CPUParticles2D").emitting=true
		$danos/Morto.show()
		$Tempo_morto2.start()
		


func _on_Tempo_morto2_timeout():
	$danos/Morto.hide()
	$danos/Morto2.show()
