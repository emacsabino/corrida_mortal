extends Node2D
var inimigo = preload("res://Scenes/Inimigo1.tscn")
var tam_grupo
var gerar_inimigos=true
var capim = preload("res://Scenes/Capim.tscn")
var pedra= preload("res://Scenes/Pedra.tscn")
var missel = preload("res://Scenes/powers/Sinal_missel.tscn")
func _ready():
	
	randomize()
		
	
	PainelSingleton.par_left=$Pista/Later_esq.get_global_position()
	PainelSingleton.par_right=$Pista/Later_dir.get_global_position()
	


	
func colidiu_parede():
	return true






func _on_Timer_timeout():
	
	tam_grupo = get_tree().get_nodes_in_group("inimigos")
	
				
	if gerar_inimigos and tam_grupo!=null and len(tam_grupo)<9:
		
		
		var escolha=rand_range(0,2)
		if int(escolha)!=0: 
			var ini = inimigo.instance()
			ini.add_to_group("inimigos")	
	
			add_child(ini)

func reduzir_velocidade_pista(valor):
	gerar_inimigos=false
	$AnimationPlayer.set_speed_scale(0.5)
	for vei in get_tree().get_nodes_in_group("inimigos"):
		if !vei.get_entrou_na_areia() and vei.get_voltar():
			vei.alterar_velocidade(-500)
		elif vei.get_entrou_na_areia() and vei.get_voltar():
			vei.alterar_velocidade(-100)
	


	
func normalizar_velocidade_pista():
	gerar_inimigos=true
	$AnimationPlayer.set_speed_scale(1)
	for vei in get_tree().get_nodes_in_group("inimigos"):
		vei.progredir_velocidade()
	
	

func gerar_pedra(posicao_x):
	var p = pedra.instance()
	var tam = rand_range(0.2,0.4)
	p.tamanho(tam)
	p.set_global_position(Vector2(posicao_x,$Pista/Pista1/VisibilityNotifier2D.get_global_position().y-300))
	$Pista/Pista1.add_child(p)

func gerar_capim(posicao_x):

	var c = capim.instance()
	var tam = rand_range(0.2,0.98)
	c.tamanho(tam)
	
		
	c.set_global_position(Vector2(posicao_x,$Pista/Pista1/VisibilityNotifier2D.get_global_position().y-300))
	$Pista/Pista1.add_child(c)


	


func _on_VisibilityNotifier2D_screen_entered():
	var escolha = [1,2]
	escolha.shuffle()
	var pos = [rand_range(-340,-260),rand_range(260,330)]	
	
	if(escolha[0]==1):	
		gerar_capim(pos[int(rand_range(0,2))])
	elif(escolha[0]==2):
		gerar_pedra(pos[int(rand_range(0,2))])
		
func escolher_alvo_aleatorio():
	pass
	var taman = get_tree().get_nodes_in_group("inimigos")
	var carr = int(rand_range(0,len(taman)))
	print(taman[carr].get_global_position())
	
	

func _on_Missel_timeout():
	var m = missel.instance()
	m.set_global_position(Vector2(200,get_global_position().y))
	add_child(m)
