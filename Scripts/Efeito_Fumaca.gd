extends Node2D

var x
func _ready():
	randomize()
	var esc = int(rand_range(1,6))
	x = rand_range(-0.3,0.4)
	var img = load("res://Assets/efeito_fumaca/"+str(esc)+".png")
	$Sprite.set_deferred("texture",img)
	$Efeito.play("sumir")

func _process(delta):
	translate(60*delta*Vector2(x,1))

func _on_Efeito_animation_finished(_anim_name):
	queue_free()
