extends Node2D
var velocidade=300
var direcao:Vector2=Vector2(0,1)
func _ready():
	randomize()
	var di_x = rand_range(-1,1)
	set_process(true)
	direcao.x=di_x
	
func _process(delta):
	translate(velocidade*direcao*delta)


func _on_Area2D_area_entered(area):
	if area.has_method("parede"):
		direcao.x=-direcao.x
