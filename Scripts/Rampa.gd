extends Area2D

var entrou = false
func _ready():
	pass # Replace with function body.


func rampa():
	pass

func _on_Rampa_area_entered(area):
	if area.has_method("heroi"):
		area.get_node("AnimeCaminhonete").set_frame(7)
		area.get_node("Sons/Salto").play()
		get_parent().get_parent().get_node("Buraco").desativar()
		entrou=true
		$Ativar_Buraco.start()

func get_entrou():
	return entrou

func ativar():
	set_collision_mask_bit(13,true)
func desativar():
	set_collision_mask_bit(13,false)


func _on_Ativar_Buraco_timeout():
	get_parent().get_parent().get_node("Buraco").ativar()


func _on_Rampa_area_exited(area):
	if area.has_method("heroi"):
		area.get_parent().get_node("EfeitosAcessorios").play("Salto")
